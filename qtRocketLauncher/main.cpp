/*
 * File:   main.cpp
 * Author: tim
 *
 * Created on 18. Juni 2013, 10:26
 */

#include <QtGui/QApplication>

#include "mainWindow.h"

int main(int argc, char *argv[]) {
    // initialize resources, if needed
    // Q_INIT_RESOURCE(resfile);

    QApplication app(argc, argv);

    // create and show your widgets here
    mainWindow *wnd = new mainWindow();
    wnd->show();

    return app.exec();
}
