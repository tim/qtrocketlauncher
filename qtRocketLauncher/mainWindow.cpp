/*
 * File:   mainWindows.cpp
 * Author: tim
 *
 * Created on 18. Juni 2013, 10:32
 */

#include "mainWindow.h"

mainWindow::mainWindow() {
    widget.setupUi(this);
    
    if (libusb_init(&ctl) != 0) {
        std::cerr << "Unable to initialize libusb!" << std::endl;
        return;
    }

    handle = libusb_open_device_with_vid_pid(ctl, 0x2123, 0x1010);
    if (!handle) {
        std::cerr << "Device not found!" << std::endl;
        return;
    }

    if (libusb_kernel_driver_active(handle, 0)) {
        std::cout << "Detaching kernel driver... ";
        libusb_detach_kernel_driver(handle, 0);
        std::cout << "okay!" << std::endl;
    }
    
    if (libusb_claim_interface(handle, 0) != 0) {
        std::cerr << "Unable to claim interface..." << std::endl;
        return;
    }
    
    connect(widget.buttonFire, SIGNAL(clicked()), this, SLOT(onFire()));
    
    ready = true;
}

mainWindow::~mainWindow() {
    if (ready) return;
        libusb_close(handle);
}

void mainWindow::onFire() {
    std::cout << "onFire()" << std::endl;
   
    if (!ready) return;
    
    send_command(CMD_FIRE);
}

void mainWindow::keyPressEvent(QKeyEvent *ev) {
    bool changed;

    if (!ready || ev->isAutoRepeat()) return;
    
    std::cout << "keyPressEvent(" << ev->key() << ")" << std::endl;
    
    switch(ev->key()) {
        
        case Qt::Key_W:
        case Qt::Key_Up:
            command |= CMD_DOWN;
            changed = true;
            break;
        
        case Qt::Key_A:
        case Qt::Key_Left:
            command |= CMD_LEFT;
            changed = true;
            break;
        
        case Qt::Key_S:
        case Qt::Key_Down:
            command |= CMD_UP;
            changed = true;
            break;

        case Qt::Key_D:
        case Qt::Key_Right:
            command |= CMD_RIGHT;
            changed = true;
            break;
        
        case Qt::Key_Space:
        case Qt::Key_Enter:
            onFire();
    }
    
    if (changed)
        send_command(command);
    changed = false;
    
}

void mainWindow::keyReleaseEvent(QKeyEvent *ev) {
    bool changed;

    if (!ready || ev->isAutoRepeat()) return;
    
    std::cout << "keyReleaseEvent(" << ev->key() << ")" << std::endl;
    
    switch(ev->key()) {
        
        case Qt::Key_W:
        case Qt::Key_Up:
            command &= CMD_DOWN ^ '\xFF';
            changed = true;
            break;
        
        case Qt::Key_A:
        case Qt::Key_Left:
            command &= CMD_LEFT ^ '\xFF';
            changed = true;
            break;
        
        case Qt::Key_S:
        case Qt::Key_Down:
            command &= CMD_UP ^ '\xFF';
            changed = true;
            break;

        case Qt::Key_D:
        case Qt::Key_Right:
            command &= CMD_RIGHT ^ '\xFF';
            changed = true;
            break;
        
    }
    
    if (changed)
        send_command(command);
    changed = false;
    
}

void mainWindow::send_command(char combined) {
    unsigned char bytes[] = "\x02\x00";
    
    if (!ready) return;
    
    std::cout << "send_command()" << std::endl;

    if (combined == 0)
        combined = CMD_STOP;
    
    bytes[1] = combined;
    
    libusb_control_transfer(handle, 0x21, 0x09, 0, 0, bytes, CMD_LEN, 0);
}

