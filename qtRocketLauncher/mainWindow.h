/* 
 * File:   mainWindows.h
 * Author: tim
 *
 * Created on 18. Juni 2013, 10:32
 */

#ifndef _MAINWINDOWS_H
#define	_MAINWINDOWS_H

#include <qt4/QtGui/QKeyEvent>
#include <iostream>
#include <libusb-1.0/libusb.h>

#include "ui_mainWindow.h"

#define CMD_LEN 2

#define CMD_DOWN  '\x01'
#define CMD_UP    '\x02'
#define CMD_LEFT  '\x04'
#define CMD_RIGHT '\x08'
#define CMD_FIRE  '\x10'
#define CMD_STOP  '\x20'

class mainWindow : public QMainWindow {
    Q_OBJECT
public:
    mainWindow();
    virtual ~mainWindow();

public slots:
    void onFire();
    void keyPressEvent(QKeyEvent *ev);
    void keyReleaseEvent(QKeyEvent *ev);
    
private:
    void send_command(char);
    Ui::mainWindows widget;
    bool ready;
    char command;
    libusb_context* ctl;
    libusb_device_handle* handle;

};

#endif	/* _MAINWINDOWS_H */

