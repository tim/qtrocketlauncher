/********************************************************************************
** Form generated from reading UI file 'mainWindow.ui'
**
** Created: Tue Jun 18 16:56:49 2013
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_mainWindows
{
public:
    QWidget *centralwidget;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *buttonFire;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *mainWindows)
    {
        if (mainWindows->objectName().isEmpty())
            mainWindows->setObjectName(QString::fromUtf8("mainWindows"));
        mainWindows->resize(661, 486);
        centralwidget = new QWidget(mainWindows);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayoutWidget = new QWidget(centralwidget);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(0, 0, 661, 461));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        buttonFire = new QPushButton(verticalLayoutWidget);
        buttonFire->setObjectName(QString::fromUtf8("buttonFire"));
        buttonFire->setMaximumSize(QSize(16777215, 16777215));

        verticalLayout->addWidget(buttonFire);

        mainWindows->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(mainWindows);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        mainWindows->setStatusBar(statusbar);

        retranslateUi(mainWindows);

        QMetaObject::connectSlotsByName(mainWindows);
    } // setupUi

    void retranslateUi(QMainWindow *mainWindows)
    {
        mainWindows->setWindowTitle(QApplication::translate("mainWindows", "qtRocketLauncher", 0, QApplication::UnicodeUTF8));
        buttonFire->setText(QApplication::translate("mainWindows", "Feuer!", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class mainWindows: public Ui_mainWindows {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
