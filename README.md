# qtRocketLauncher
This application controls the Thunder Missile Launcher and Storm OIC Launcher under Linux. It it based on the libusb and runs entirely in user space.

## Prepare
Make sure libqt4 and libusb are installed.

	sudo apt-get install libqt4-dev libusb-dev

## Compile

	cd qtRocketLauncher
	cmake .
	make

optional:

	sudo make install

## Run

	./qtRocketLauncher

If you get the message "Device not found..." on the terminal the application was unable to identify the rocket launcher, make sure it is plugged in.

## Controls
Use WASD to adjust the direction. Press "Feuer!"-Button or Space to fire a rocket.

Have fun!
