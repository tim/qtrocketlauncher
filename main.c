/* 
 * File:   main.c
 * Author: tim
 *
 * Created on 17. Juni 2013, 18:07
 */

///*

#include <stdio.h>
#include <stdlib.h>
#include <libusb-1.0/libusb.h>

#define CMD_LEN 8

#define CMD_DOWN  '\x01'
#define CMD_UP    '\x02'
#define CMD_LEFT  '\x04'
#define CMD_RIGHT '\x08'
#define CMD_FIRE  '\x10'
#define CMD_STOP  '\x20'

void send_command(libusb_device_handle*, char);

int main(int argc, char** argv) {
    
    libusb_context* ctl = NULL;
    libusb_device_handle* handle;
    int timeout = 500;
    int actual_len, exit = 0;
    char buffer;
    
    if (libusb_init(&ctl) != 0) {
        fprintf(stderr, "Unable to initialize libusb...\n");
        return EXIT_FAILURE;
    }

    handle = libusb_open_device_with_vid_pid(ctl, 0x2123, 0x1010);
    if (!handle) {
        fprintf(stderr, "Device not found...\n");
        return EXIT_FAILURE;        
    }
    
    if (libusb_kernel_driver_active(handle, 0)) {
        fprintf(stdout, "Detaching kernel driver... ");
        libusb_detach_kernel_driver(handle, 0);
        fprintf(stdout, "okay\n");
    }
    
    if (libusb_claim_interface(handle, 0) != 0) {
        fprintf(stderr, "Unable to claim interface...\n");
        return EXIT_FAILURE;        
    }
    
    while (!exit) {
        scanf("%c", &buffer);
        
        switch(buffer) {
            case 'l':
                send_command(handle, CMD_LEFT);
                break;
                
            case 'r':
                send_command(handle, CMD_RIGHT);
                break;

            case 'u':
                send_command(handle, CMD_UP);
                break;

            case 'd':
                send_command(handle, CMD_DOWN);
                break;

            case 'f':
                send_command(handle, CMD_FIRE);
                break;

            case 'e':
                send_command(handle, CMD_STOP);
                libusb_close(handle);
                return;
                
            case 's':
                send_command(handle, CMD_STOP);
        }
        
    }
    

    fprintf(stdout, "Finished...\n");
    return EXIT_SUCCESS;
}

void send_command(libusb_device_handle* handle, char combined) {
    char bytes[] = "\x02\x00\x00\x00\x00\x00\x00\x00";
    bytes[1] = combined;
    
    libusb_control_transfer(handle, 0x21, 0x09, 0, 0, bytes, CMD_LEN, 0);
}

//*/